# poi_template

This repo stores the POI dashboard in Tableau.

Template use "Century Gothic Regular" font, you have to install it into your computer before start working.

# New POI's WORKFLOW
1. Install font in your local computer
2. Clone the repo into your local computer
3. Copy the POI_Template.twbx file to your POI working folder

## WARNING:

*Do not work/save any changes in the repo folder (the one you cloned locally - step #2 - )*

# Update POI template WORKFLOW
1. Install font in your local computer
2. Clone the repo into your local computer
3. Change branch to "dev"
4. Do your changes
5. Once you have a final version, merge dev branch to master branch to make available the latest version into production.
6. If use a newest tableau version please update the line below


Template Tableau Version: 10.4.2
